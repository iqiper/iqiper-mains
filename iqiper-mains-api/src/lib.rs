/// Contains diverses structures used to extract the open api data from a json configuration file.
/// 
/// The most important is `OpenApiDocument` which is the root of the api spec, all the other structures are sub sections of this one.  
/// This  main structure should be wrapped as a [`Data`] of the actix server in order to make the authorization possible.  
/// The functions [`build_api`] and [`read_api_file`] will parse for you the structure from a `json` file.  
/// 
/// [`Data`]: https://docs.rs/actix-web/2.0.0-rc/actix_web/web/struct.Data.html
/// [`build_api`]: ../fn.build_api.html
/// [`read_api_file`]: ../fn.read_api_file.html
pub mod api;