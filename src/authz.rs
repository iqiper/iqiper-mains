use crate::{api, AuthzError};
use actix_web::{dev::ServiceRequest, Error};

use chrono::{serde::ts_seconds, serde::ts_seconds_option, DateTime, Utc};
use iqiper_actix_jwt_extractor::CoreJWTAuth;

use serde::{Deserialize, Serialize};
const MISSING_SCOPE: &str = "Insufficient permissions to access this ressource";

/// The claims stored inside the JWT. Used to verify authenticity, user identity
/// as well as user authorizations.
#[derive(Debug, Deserialize, Serialize)]
pub struct JWTClaimsAndBaseFields {
    // The JWT audience
    pub aud: String,
    // The expiration date of the token
    #[serde(with = "ts_seconds_option")]
    pub exp: Option<DateTime<Utc>>,
    // The issued date of the token
    #[serde(with = "ts_seconds")]
    pub iat: DateTime<Utc>,
    // The issuer of the token
    pub iss: String,
    // The "not before" field, stating that the token should be considered
    // active before this date
    #[serde(with = "ts_seconds_option")]
    pub nbf: Option<DateTime<Utc>>,
    // The token subject
    pub sub: String,
    // The token scopes, whitespace separated
    pub scope: String,
}

/// The claims stored inside the JWT. Used to verify authenticity, user identity
/// as well as user authorizations.
#[derive(Debug, Deserialize, Serialize)]
pub struct JWTClaims {
    pub sub: uuid::Uuid,
    // The token scopes, whitespace separated
    pub scope: String,
}

/// Check that at least one of the claims' scopes is contained in the securities' scopes.
///
/// # Arguments
/// * `securities` - The list of scopes allowed.
/// * `claims` - The claims requiring autorization.
///
/// # Return Value
/// May return an error if none of the claims' scope if contained by `securities`.
pub fn validate_scope(securities: &Vec<String>, claims: &JWTClaims) -> Result<(), AuthzError> {
    if securities.len() == 0 {
        return Ok(());
    }
    for scope in claims.scope.split_whitespace() {
        if securities.contains(&scope.to_owned()) {
            return Ok(());
        }
    }
    Err(AuthzError::MissingScope(
        MISSING_SCOPE.to_owned(),
        securities.clone(),
    ))
}

/// Clone all the scopes into a single Vector.
///
/// This aims to make it easier to iterate over all the scopes of an `OpenAPISecurityScope`.
///
/// # Arguments
/// * `securities` - A ref to the `OpenAPISecurityScope` that will be flatten.
///
/// # Return Value
/// All the scopes, cloned into a single vector.
pub fn flatten_securities(securities: &api::OpenAPISecurityScope) -> Vec<String> {
    let mut flatten = Vec::new();
    if let Some(sec_vec) = securities {
        for map in sec_vec {
            for (_key, stack) in map {
                for scope in stack {
                    if !flatten.contains(scope) {
                        flatten.push(scope.clone());
                    }
                }
            }
        }
    }
    return flatten;
}

/// Used as middleware to validate or not any request on a route.
///
/// This function is used by [`route_validation!`], so it should not be set manually at all.
///
/// # Arguments
/// * `req` - The req to validate.
/// * `credentials` - The credentials of the request.
/// * `route` - The route protected by the middleware.
/// * `method` - The method protected by the middleware.
///
/// # Return Value
/// The request if it was validated, an actix error otherwise.
///
/// [`route_validation!`]: ./attr.route_validation.html
pub fn validator(
    req: ServiceRequest,
    token: CoreJWTAuth<JWTClaims>,
    route: &str,
    method: &str,
) -> Result<ServiceRequest, Error> {
    let api_doc: &api::OpenAPIDocument = req.app_data().ok_or(AuthzError::auth_not_configured())?;
    let op = api_doc
        .paths()
        .get(route)
        .ok_or(AuthzError::route_not_defined(route, method))?
        .operation(method)
        .as_ref()
        .ok_or(AuthzError::route_not_defined(route, method))?;
    let securities = flatten_securities(op.securities());
    validate_scope(&securities, &token.jwt().claims)?;
    Ok(req)
}
