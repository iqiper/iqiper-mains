use actix_web::{
    error,
    http::{header, StatusCode},
    HttpResponse,
};
use std::fmt;

/// Diverse kind of error that can be returned as json by the server if the authz fail.
#[derive(Debug, PartialEq)]
pub enum AuthzError {
    /// A single message indicating if the request is incorrect.
    BadRequest(String),
    /// A single message indicating if any step of the authz failed.
    Unauthorized(String),
    /// A message specifying the scopes required for the authz.
    MissingScope(String, Vec<String>),
}

impl AuthzError {
    /// Generate an error message for the user about a missing header.
    ///
    /// # Arguments
    /// * `header_name` - The name of the missing header.
    ///
    /// # Return Value
    /// The response error ready to be sent to the user.
    pub fn auth_not_configured() -> Self {
        AuthzError::Unauthorized(String::from("This server is not configured correctly"))
    }

    /// Generate an error message for the user about a missing header.
    ///
    /// # Arguments
    /// * `header_name` - The name of the missing header.
    ///
    /// # Return Value
    /// The response error ready to be sent to the user.
    pub fn route_not_defined(route: &str, method: &str) -> Self {
        AuthzError::Unauthorized(format!(
            "The `{}` route is not defined for the method '{}'",
            route, method
        ))
    }

    /// Generate an error message for the user about a missing header.
    ///
    /// # Arguments
    /// * `header_name` - The name of the missing header.
    ///
    /// # Return Value
    /// The response error ready to be sent to the user.
    pub fn missing_header(header_name: &str) -> Self {
        AuthzError::Unauthorized(format!(
            "An `{}` header is required to access this ressource",
            header_name
        ))
    }

    /// Generate an error message for the user about an incorrect header.
    ///
    /// # Arguments
    /// * `header_name` - The name of the incorrect header.
    ///
    /// # Return Value
    /// The response error ready to be sent to the user.
    pub fn incorrect_header(header_name: &str) -> Self {
        AuthzError::Unauthorized(format!("The `{}` header is incorrect", header_name))
    }

    /// Generate an error message for the user about an incorrect jwt.
    ///
    /// # Arguments
    /// * `details` - An explanation of why the token is invalid.
    ///
    /// # Return Value
    /// The response error ready to be sent to the user.
    pub fn incorrect_jwt(details: &str) -> Self {
        AuthzError::Unauthorized(format!("The JWT is incorrect: {}", details))
    }

    /// Generate a json value for serde from the error.
    ///
    /// # Return Value
    /// A json value for serde.
    fn json_body(&self) -> serde_json::Value {
        match self {
            AuthzError::BadRequest(msg) => serde_json::json!({
                "error": {
                    "code": 400,
                    "message": msg
                }
            }),
            AuthzError::Unauthorized(msg) => serde_json::json!({
                "error": {
                    "code": 401,
                    "message": msg
                }
            }),
            AuthzError::MissingScope(msg, scopes) => serde_json::json!({
                "error": {
                    "code": 401,
                    "message": msg,
                    "wanted_scopes": scopes
                }
            }),
        }
    }
}

impl fmt::Display for AuthzError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.json_body())
    }
}

impl error::ResponseError for AuthzError {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code())
            .set_header(header::CONTENT_TYPE, "application/json")
            .json(self.json_body())
    }

    fn status_code(&self) -> StatusCode {
        match self {
            AuthzError::BadRequest(_) => StatusCode::BAD_REQUEST,
            _ => StatusCode::UNAUTHORIZED,
        }
    }
}
