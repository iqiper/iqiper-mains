//! `iqiper-mains` is a lib to handle authorization and authentication for `iqiper`
//!
//! ## Configuration
//!
//! This crate expose a configuration to validate the JWT passed to the API server.
//!
//! ## JWT Extractor
//!
//! It exposes an extractor for `actix` to parse the JWT from an HTTP request.
//! It'll then validate the tokens using the config passed as `app_data`.

#[doc(hidden)]
mod authz;
pub use authz::{validator, JWTClaims, JWTClaimsAndBaseFields};

#[doc(hidden)]
mod authn;
pub use authn::{
    forward_authn_token, forward_authn_token_form, forward_authn_token_query, query_handler,
    QueryGrant,
};

#[doc(hidden)]
mod authz_error;
pub use authz_error::AuthzError;

#[doc(hidden)]
mod data;
pub use data::{
    auth_config::AuthConfig,
    auth_settings::{AuthSettings, IdpSettings, JwtSettings, LocalServerSettings},
    jwks::{refresh_jwks, request_jwks},
    jwks::{JsonWebKeyStoreUnparsed, JsonWebKeyUnparsed},
};
pub use iqiper_actix_jwt_extractor::JsonWebKeyStore;

pub use iqiper_actix_jwt_extractor as jwt_extractor;

pub use jsonwebtoken as jwt;

extern crate macros;
pub use macros::iqiper_mains_route;

pub use actix_web_httpauth;

pub use jsonwebtoken::Algorithm;
extern crate open_api;
pub use open_api::api;
#[cfg(test)]
mod tests;
