use super::auth_settings;
use iqiper_actix_jwt_extractor::CoreJWTConfig;
use iqiper_actix_jwt_extractor::JsonWebKeyStore;
/// The configuration for the authentication.
///
/// The json web keys and the validation are behind an async reference counter and a read and write lock.
/// This wrapping allow us to update the keys during the service execution without having to reload it.
#[derive(Clone)]
pub struct AuthConfig<'a> {
    // The route to get the JWT public keys
    pub settings: auth_settings::AuthSettings,
    // The JWT validation configuration
    pub jwt_config: CoreJWTConfig<'a>,
    // The HTTP client to use
    pub reqwest_client: reqwest::Client,
}

impl<'a> AuthConfig<'a> {
    /// Create a new authentication configuration.
    ///
    /// # Arguments
    /// * `settings` - The settings required by the auth.
    /// * `jwks` - The json web key vector to lock for multithread use.
    ///
    /// # Return Value
    /// The new authentication configuration.
    pub fn new(
        settings: auth_settings::AuthSettings,
        jwks: JsonWebKeyStore<'a>,
        reqwest_client: reqwest::Client,
    ) -> Self {
        let mut validation = jsonwebtoken::Validation {
            validate_exp: true,
            validate_nbf: true,
            iss: Some(settings.jwt.issuer.clone()),
            algorithms: settings.jwt.algo.clone(),
            leeway: settings.jwt.leeway.unwrap_or(0) as u64,
            ..jsonwebtoken::Validation::default()
        };
        validation.set_audience(&settings.jwt.audience);
        AuthConfig {
            settings,
            jwt_config: CoreJWTConfig::new(validation, jwks),
            reqwest_client,
        }
    }

    /// Getter for the settings of the server.
    ///
    /// # Return Value
    /// A result with a guard with a read access to the JWKS.
    pub fn settings(&self) -> &auth_settings::AuthSettings {
        &self.settings
    }

    /// Getter for the JWT configuration.
    ///
    /// # Return Value
    /// The JWT config.
    pub fn jwt_config(&self) -> &CoreJWTConfig<'a> {
        &self.jwt_config
    }

    /// Getter for the HTTP Client
    ///
    /// # Return Value
    /// The HTTP Client.
    pub fn reqwest_client(&self) -> &reqwest::Client {
        &self.reqwest_client
    }
}
