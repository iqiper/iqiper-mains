use crate::AuthConfig;
use actix_web::{web, HttpResponse, Responder};
use iqiper_actix_jwt_extractor::JWTConfig;
use iqiper_actix_jwt_extractor::JsonWebKeyStore;
use serde::{Deserialize, Serialize};

/// Used to deserialize the list of json web keys from the response.
#[derive(Default, Clone, Debug, Serialize, Deserialize, PartialEq)]
pub struct JsonWebKeyStoreUnparsed {
    /// The list of json web keys
    pub keys: Vec<JsonWebKeyUnparsed>,
}

/// Describe a JWT Key that can be used to verify token.
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct JsonWebKeyUnparsed {
    /// Identifier of the key.
    pub kid: String,
    /// The key type.
    pub kty: String,
    /// The algorithm used.
    pub alg: String,
    /// The key purpose (renamed field for `use` which is also a rust keyword).
    #[serde(rename = "use")]
    pub usecase: String,
    /// The modulus (public).
    pub n: String,
    /// The exponent (public).
    pub e: String,
}

/// Request a new vector of JWKS from keycloack.
///
/// # Arguments
/// * `reqwest_client` - The reqwest client
/// * `route` - The route to get JWKS
///
/// # Return Value
/// An empty vector is returned if the request to keycloack failed, a vector with the new keys otherwise.
/// If the content of the response cannot be deserialized correctly an empty vector is returned.
pub async fn request_jwks<'a>(
    reqwest_client: &reqwest::Client,
    route: &String,
) -> JsonWebKeyStore<'a> {
    let rep = reqwest_client.get(route).send().await;
    match rep {
        Ok(res) => match res.json::<JsonWebKeyStoreUnparsed>().await {
            Ok(store) => {
                let mut new_store: JsonWebKeyStore<'a> = Default::default();
                for key in store.keys.iter() {
                    new_store.keys.insert(
                        key.kid.clone(),
                        jsonwebtoken::DecodingKey::from_rsa_components(&key.n, &key.e)
                            .into_static(),
                    );
                }
                new_store
            }
            Err(_err) => Default::default(),
        },
        Err(err) => {
            log::error!("Failed to retrieve jwks: {}", err);
            Default::default()
        }
    }
}

/// Update the values of the jwks stored in the server state.
///
/// Panic if the jwks lock is poisoned.
///
/// # Arguments
/// * `data` - The server state shared between all thread of the service.
///
/// # Return Value
/// A `200` response if the refresh succeed, a `500` otherwise.
pub async fn refresh_jwks<'a>(data: web::Data<AuthConfig<'a>>) -> impl Responder {
    let jwks = request_jwks(data.reqwest_client(), &data.settings().idp.jwks_route).await;
    let is_fine = jwks.keys.len() > 0;
    *data
        .jwt_config()
        .jwks()
        .write()
        .expect("The JWKS lock is poisoned") = jwks;
    if is_fine {
        HttpResponse::Ok()
    } else {
        HttpResponse::InternalServerError()
    }
}
