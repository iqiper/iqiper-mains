use jsonwebtoken::Algorithm;
use serde::{Deserialize, Serialize};

/// Settings used for auth.
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct AuthSettings {
    pub idp: IdpSettings,
    pub jwt: JwtSettings,
    pub local_server: LocalServerSettings,
}

/// Configuration relative to the JWT
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct JwtSettings {
    /// The audience of the JWT
    pub audience: Vec<String>,
    /// The signature algorithms of JWT.
    pub algo: Vec<Algorithm>,
    /// The organism from where the tokens come from.
    pub issuer: String,
    /// The clock skew to apply
    pub leeway: Option<i64>,
}

/// Configuration relative to the Keycloack server.
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct LocalServerSettings {
    // The service-mains host
    pub host: String,
    // The service-mains port
    pub port: u32,
    // The uri to advertise
    pub advertise_uri: String,
    // The scheme to advertise with the uri
    pub advertise_scheme: String,
    // Local path to handle authorization code
    pub local_code_route: String,
}

/// Configuration to setup how service-mains should identify itself to the IdP
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct IdpSettings {
    // The client id in keycloak
    pub client_id: String,
    // The client secret in keycloak
    pub client_secret: String,
    // The route to send the authorization code to
    pub token_route: String,
    // The route to get the JWT public keys
    pub jwks_route: String,
}
