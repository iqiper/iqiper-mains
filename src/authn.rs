use crate::{AuthConfig, AuthzError};
use actix_web::web::QueryConfig;
use actix_web::{web, HttpResponse};
use serde::{Deserialize, Serialize};

/// Define the query extrc=actor type for the ACF parameters.
pub type QueryGrant = actix_web::web::Query<IqiperGrantParameter>;

/// Parameters send by the user to get an authorization from the IdP.
#[derive(Debug, Deserialize)]
pub struct AuthorizationCodeFlowParameters {
    /// The session state joined with the authorization code.
    /// This state will be sent back from the IdP
    state: Option<String>,
    /// The authorization code issued by the IdP. This token can be exchanged
    /// for ID, Access and Refresh tokens
    code: String,
    /// The PKCE code verifier, if used
    code_verifier: Option<String>,
    /// Replace the default redirect URI.
    redirect_uri: Option<String>,
}

/// Parameters send by the user to get an refresh token from the IdP.
#[derive(Debug, Deserialize)]
pub struct RefreshTokenFlowParameters {
    /// The refresh token to use to exchange with a new access, refresh and id token.
    refresh_token: String,
}

/// Parameters send by the user to get an token from the IdP.
#[derive(Debug, Deserialize)]
pub struct PasswordFlowParameters {
    /// The user's username
    username: String,
    /// The user's password
    password: String,
    /// The scopes requested
    scope: String,
}

/// Empty form
#[derive(Debug, Deserialize)]
pub struct EmptyForm {}

/// Supported parameter for this route. Either to pass along a `authorization_code`
/// or a `refresh_token` in exchange for new tokens
#[derive(Debug, Deserialize)]
#[serde(untagged)]
#[serde(rename = "snake_case")]
pub enum IqiperGrantParameter {
    #[allow(non_camel_case_types)]
    authorization_code(AuthorizationCodeFlowParameters),
    #[allow(non_camel_case_types)]
    refresh_token(RefreshTokenFlowParameters),
    #[allow(non_camel_case_types)]
    password(PasswordFlowParameters),
    #[allow(non_camel_case_types)]
    empty(EmptyForm),
}

/// The reponse enumeration, depending on `grant_type`
#[derive(Debug, Serialize)]
#[serde(tag = "grant_type")]
#[serde(rename = "snake_case")]
pub enum IqiperGrantForm<'a> {
    #[allow(non_camel_case_types)]
    authorization_code(AuthorizationCodeFlowForm<'a>),
    #[allow(non_camel_case_types)]
    refresh_token(RefreshTokenFlowForm<'a>),
    #[allow(non_camel_case_types)]
    password(PasswordFlowForm<'a>),
}

/// Configure the query extractor to return any error into a json.
///
/// # Arguments
/// * `cfg` - the Query extractor configuration that will be modified.
///
/// # Return Value
/// The query extractor configuration updated.
pub fn query_handler(cfg: QueryConfig) -> QueryConfig {
    cfg.error_handler(|err, _| AuthzError::BadRequest(err.to_string()).into())
}

/// A form ready to be sent to the IdP to request authorization.
#[derive(Debug, Serialize)]
#[serde(rename = "snake_case")]
pub struct AuthorizationCodeFlowForm<'a> {
    /// The code that was produced by the IdP. To exchange for tokens
    code: String,
    /// The PKCE code verifier, if used
    code_verifier: Option<String>,
    /// The client id of service-mains
    client_id: &'a String,
    /// The client secret of service-mains
    client_secret: &'a String,
    /// The uri on which the IdP should redirect the end-user after the flow
    /// is complete
    redirect_uri: String,
}

/// A form ready to be sent to the IdP to request refresh token.
#[derive(Debug, Serialize)]
pub struct RefreshTokenFlowForm<'a> {
    /// The code that was produced by the IdP. To exchange for tokens
    refresh_token: String,
    /// The client id of service-mains
    client_id: &'a String,
    /// The client secret of service-mains
    client_secret: &'a String,
}

/// A form ready to be sent to the IdP to request token.
#[derive(Debug, Serialize)]
pub struct PasswordFlowForm<'a> {
    /// The user's username
    username: String,
    /// The user's password
    password: String,
    /// The scopes requested
    scope: String,
    /// The client id of service-mains
    client_id: &'a String,
    /// The client secret of service-mains
    client_secret: &'a String,
}

/// Forward a request for authorization from the user to keycloak.
///
/// # Arguments
/// * `data` - the service state wraped in a [`Data`] type.
/// * `acf_parameter` - parameters sent by user and  used to create a form for keycloak.
///
/// # Return Value
/// If keycloak manage to handle the request, its Response is forwarded to the user.  
/// Otherwise the function return an internal error to the user.
///
/// [`Data`]: https://docs.rs/actix-web/2.0.0-rc/actix_web/web/struct.Data.html
pub async fn forward_authn_token<'a>(
    data: &AuthConfig<'a>,
    request: IqiperGrantParameter,
) -> HttpResponse {
    let settings = &data.settings();
    let form: IqiperGrantForm<'_> = match request {
        IqiperGrantParameter::authorization_code(acf_param) => {
            IqiperGrantForm::authorization_code(AuthorizationCodeFlowForm {
                code: acf_param.code,
                code_verifier: acf_param.code_verifier,
                client_id: &settings.idp.client_id,
                client_secret: &settings.idp.client_secret,
                redirect_uri: acf_param.redirect_uri.clone().unwrap_or(format!(
                    "{}://{}:{}{}",
                    settings.local_server.advertise_scheme,
                    settings.local_server.advertise_uri,
                    settings.local_server.port,
                    settings.local_server.local_code_route,
                )),
            })
        }
        IqiperGrantParameter::refresh_token(rt_param) => {
            IqiperGrantForm::refresh_token(RefreshTokenFlowForm {
                refresh_token: rt_param.refresh_token,
                client_id: &settings.idp.client_id,
                client_secret: &settings.idp.client_secret,
            })
        }
        IqiperGrantParameter::password(p_param) => IqiperGrantForm::password(PasswordFlowForm {
            username: p_param.username,
            password: p_param.password,
            scope: p_param.scope,
            client_id: &settings.idp.client_id,
            client_secret: &settings.idp.client_secret,
        }),
        _ => return HttpResponse::BadRequest().finish(),
    };
    let res = data
        .reqwest_client()
        .post(&settings.idp.token_route)
        .header(
            reqwest::header::CONTENT_TYPE,
            reqwest::header::HeaderValue::from_static("application/x-www-form-urlencoded"),
        )
        .form(&form)
        .send()
        .await;
    match res {
        Ok(resp) => {
            let status = resp.status();
            let bytes = resp.bytes().await;

            if bytes.is_ok() && !status.is_server_error() {
                HttpResponse::build(status).body(bytes.unwrap())
            } else {
                HttpResponse::ServiceUnavailable().finish()
            }
        }
        Err(_err) => HttpResponse::ServiceUnavailable().finish(),
    }
}

pub async fn forward_authn_token_query<'a>(
    data: web::Data<AuthConfig<'a>>,
    param: web::Query<IqiperGrantParameter>,
    // body: web::Form<IqiperGrantParameter>,
) -> HttpResponse {
    forward_authn_token(&data.into_inner(), param.into_inner()).await
}

pub async fn forward_authn_token_form<'a>(
    data: web::Data<AuthConfig<'a>>,
    param: web::Form<IqiperGrantParameter>,
    // body: web::Form<IqiperGrantParameter>,
) -> HttpResponse {
    forward_authn_token(&data.into_inner(), param.into_inner()).await
}
