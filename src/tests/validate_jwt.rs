use super::*;
use crate::authz::{validate_jwt, JWTClaimsAndBaseFields};
use chrono::{Duration, Utc};
use std::ops::{Add, Sub};

#[test]
fn not_before() {
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::RS512);
    header.kid = Some("test_key".to_owned());
    let claims = JWTClaimsAndBaseFields {
        aud: String::from("iqiper"),
        iss: String::from("iqiper"),
        iat: Utc::now(),
        nbf: Some(Utc::now().add(Duration::days(5))),
        exp: Some(Utc::now().add(Duration::days(6))),
        sub: String::from("user"),
        scope: String::from("openid"),
    };
    let key = get_jwt_private_key();
    let jwk = gen_json_web_key(String::from("RS512"));
    let token = jsonwebtoken::encode(&header, &claims, &key).expect("Failed encoding");
    let settings = super::extract_settings(super::SETTINGS);
    let conf = crate::AuthConfig::new(settings, Vec::new());
    let res = validate_jwt(&conf.validation(), &jwk, &token);
    assert!(res.is_err(), "Token too young to be used");
}

#[test]
fn not_after() {
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::RS512);
    header.kid = Some("test_key".to_owned());
    let claims = JWTClaimsAndBaseFields {
        aud: String::from("iqiper"),
        iss: String::from("iqiper"),
        iat: Utc::now(),
        nbf: Some(Utc::now().sub(Duration::days(2))),
        exp: Some(Utc::now().sub(Duration::days(1))),
        sub: String::from("user"),
        scope: String::from("openid"),
    };
    let key = get_jwt_private_key();
    let jwk = gen_json_web_key(String::from("RS512"));
    let token = jsonwebtoken::encode(&header, &claims, &key).expect("Failed encoding");
    let settings = super::extract_settings(super::SETTINGS);
    let conf = crate::AuthConfig::new(settings, Vec::new());
    let res = validate_jwt(&conf.validation(), &jwk, &token);
    assert!(res.is_err(), "Token expired should not validate");
}

#[test]
fn issuer() {
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::RS512);
    header.kid = Some("test_key".to_owned());
    let claims = JWTClaimsAndBaseFields {
        aud: String::from("iqiper"),
        iss: String::from("amzn"),
        iat: Utc::now(),
        nbf: Some(Utc::now().sub(Duration::days(1))),
        exp: Some(Utc::now().add(Duration::days(1))),
        sub: String::from("user"),
        scope: String::from("openid"),
    };
    let key = get_jwt_private_key();
    let jwk = gen_json_web_key(String::from("RS512"));
    let token = jsonwebtoken::encode(&header, &claims, &key).expect("Failed encoding");
    let settings = super::extract_settings(super::SETTINGS);
    let conf = crate::AuthConfig::new(settings, Vec::new());
    let res = validate_jwt(&conf.validation(), &jwk, &token);
    assert!(res.is_err(), "Issuer not matching should return an error");
}

#[test]
fn algorithms() {
    let header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::RS256);
    let claims = JWTClaimsAndBaseFields {
        aud: String::from("iqiper"),
        iss: String::from("iqiper"),
        iat: Utc::now(),
        nbf: Some(Utc::now().sub(Duration::days(1))),
        exp: Some(Utc::now().add(Duration::days(1))),
        sub: String::from("user"),
        scope: String::from("openid"),
    };
    let key = get_jwt_private_key();
    let jwk = gen_json_web_key(String::from("RS512"));
    let token = jsonwebtoken::encode(&header, &claims, &key).expect("Failed encoding");
    let settings = super::extract_settings(super::SETTINGS);
    let conf = crate::AuthConfig::new(settings, Vec::new());
    let res = validate_jwt(&conf.validation(), &jwk, &token);
    assert!(
        res.is_err(),
        "Algorithm not matching should return an error"
    );
}

#[test]
fn bad_token() {
    let jwk = gen_json_web_key(String::from("RS256"));
    let settings = super::extract_settings(super::SETTINGS);
    let conf = crate::AuthConfig::new(settings, Vec::new());
    let res = validate_jwt(&conf.validation(), &jwk, "blabla");
    assert!(res.is_err(), "JWT is garbage should return an error");
}

#[test]
fn ok() {
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::RS512);
    header.kid = Some("test_key".to_owned());
    let claims = JWTClaimsAndBaseFields {
        aud: String::from("iqiper"),
        iss: String::from("iqiper"),
        iat: Utc::now(),
        nbf: Some(Utc::now().sub(Duration::days(1))),
        exp: Some(Utc::now().add(Duration::days(1))),
        sub: String::from("user"),
        scope: String::from("openid"),
    };
    let key = get_jwt_private_key();
    let jwk = gen_json_web_key(String::from("RS512"));
    let token = jsonwebtoken::encode(&header, &claims, &key).expect("Failed encoding");
    let settings = super::extract_settings(super::SETTINGS);
    let conf = crate::AuthConfig::new(settings, Vec::new());
    let res = validate_jwt(&conf.validation(), &jwk, &token);
    assert!(res.is_ok(), "A correct token should be approved");
}
