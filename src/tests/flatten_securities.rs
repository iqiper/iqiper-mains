use crate::authz::flatten_securities;
use std::collections::HashMap;

#[test]
fn empty() {
    assert_eq!(
        flatten_securities(&Some(Vec::new())),
        Vec::<String>::new(),
        "An empty vec should not create any scope"
    );
    let mut sec = Vec::new();
    sec.push(HashMap::new());
    assert_eq!(
        flatten_securities(&Some(sec)),
        Vec::<String>::new(),
        "Empty map should not create any scope"
    );
    let mut sec = Vec::new();
    let mut map = HashMap::new();
    map.insert(String::new(), Vec::new());
    sec.push(map);
    assert_eq!(
        flatten_securities(&Some(sec)),
        Vec::<String>::new(),
        "Empty vec of scopes should not create any scope"
    );
}

#[test]
fn single() {
    let mut sec = Vec::new();
    let mut map = HashMap::new();
    map.insert(String::from("alone"), vec![String::from("I am so lonely")]);
    sec.push(map);
    assert_eq!(
        flatten_securities(&Some(sec)),
        vec![String::from("I am so lonely")],
        "A lonely scope should remain alone"
    );
}

#[test]
fn multiple() {
    let mut sec = Vec::new();
    let mut map_1 = HashMap::new();
    map_1.insert(
        String::from("tim"),
        vec![String::from("having"), String::from("fun")],
    );
    map_1.insert(
        String::from("tom"),
        vec![String::from("with"), String::from("other")],
    );
    let mut map_2 = HashMap::new();
    map_2.insert(
        String::from("lea"),
        vec![String::from("people"), String::from("is")],
    );
    map_2.insert(String::from("leo"), vec![String::from("nice")]);
    sec.push(map_1);
    sec.push(map_2);
    let expected = vec![
        String::from("having"),
        String::from("fun"),
        String::from("with"),
        String::from("other"),
        String::from("people"),
        String::from("is"),
        String::from("nice"),
    ];
    let flatten = flatten_securities(&Some(sec));
    assert_eq!(expected.len(), flatten.len(), "Scope count is incorrect");
    for word in &expected {
        assert!(flatten.contains(word), "A scope is missing");
    }
}

#[test]
fn doublon() {
    let mut sec = Vec::new();
    let mut map = HashMap::new();
    map.insert(
        String::from("A"),
        vec![String::from("hello"), String::from("Mr.")],
    );
    map.insert(
        String::from("C"),
        vec![String::from("Mr."), String::from("toast")],
    );
    sec.push(map);
    let expected = vec![
        String::from("hello"),
        String::from("Mr."),
        String::from("toast"),
    ];
    let flatten = flatten_securities(&Some(sec));
    assert_eq!(expected.len(), flatten.len(), "Scope count is incorrect");
    for word in &expected {
        assert!(flatten.contains(word), "A scope is missing");
    }
}
