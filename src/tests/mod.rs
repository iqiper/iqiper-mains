// mod find_jwk_from_token;
mod flatten_securities;
mod request_jwks;
// mod validate_jwt;
mod validate_scope;

pub const SETTINGS: &str = "IQIPER_MAINS_CONFIG";

fn extract_settings(var_name: &str) -> crate::AuthSettings {
    let path = std::env::var(var_name).expect("Env var is missing");
    let file = std::fs::File::open(&path).expect("File is missing");
    let reader = std::io::BufReader::new(file);
    serde_yaml::from_reader(reader).expect("Deserialization failed")
}
