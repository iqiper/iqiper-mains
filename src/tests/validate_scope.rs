use crate::authz::{validate_scope, JWTClaims};

#[test]
fn no_scopes_in_securities() {
    let sec = Vec::new();
    let claims = JWTClaims {
        sub: uuid::Uuid::new_v4(),
        scope: String::from("access h3r3 4nd n0w"),
    };
    let res = validate_scope(&sec, &claims);
    assert!(res.is_ok(), "Should permit access when securities is empty");
}

#[test]
fn no_scope_in_claims() {
    let sec = vec![
        String::from("access"),
        String::from("h3r3"),
        String::from("4nd"),
        String::from("n0w"),
    ];
    let claims = JWTClaims {
        sub: uuid::Uuid::new_v4(),
        scope: String::new(),
    };
    let res = validate_scope(&sec, &claims);
    assert!(
        res.is_err(),
        "Should not permit access when claims do not have any scope"
    );
}

#[test]
fn not_ok() {
    let sec = vec![String::from("g3t"), String::from("l0st")];
    let claims = JWTClaims {
        sub: uuid::Uuid::new_v4(),
        scope: String::from("access h3r3 4nd n0w"),
    };
    let res = validate_scope(&sec, &claims);
    assert!(res.is_err(), "Should not permit access when no scope match");
}

#[test]
fn ok() {
    let sec = vec![
        String::from("g3t"),
        String::from("l0st"),
        String::from("not"),
        String::from("h3r3"),
    ];
    let claims = JWTClaims {
        sub: uuid::Uuid::new_v4(),
        scope: String::from("access h3r3 4nd n0w"),
    };
    let res = validate_scope(&sec, &claims);
    assert!(res.is_ok(), "Should permit access when a scope match");
}
