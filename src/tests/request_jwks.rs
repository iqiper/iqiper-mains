use crate::data::jwks::{request_jwks, JsonWebKeyStoreUnparsed, JsonWebKeyUnparsed};
use iqiper_actix_jwt_extractor::JsonWebKeyStore;
use std::sync::{Arc, Mutex};

#[actix_rt::test]
async fn not_ok() {
    let mut settings = super::extract_settings(super::SETTINGS);
    settings.idp.jwks_route = String::from("http://localhost:1/.idp/token");
    let response = request_jwks(&reqwest::Client::new(), &settings.idp.jwks_route).await;
    assert_eq!(
        response.keys.len(),
        0,
        "Should return an empty vec if request failed"
    );
}

#[actix_rt::test]
async fn empty() {
    let call_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let counter_clone = call_counter.clone();
    let mut settings = super::extract_settings(super::SETTINGS);
    let settings_srv = settings.clone();
    let srv = actix_web::test::start(move || {
        actix_web::App::new().data(counter_clone.clone()).service(
            actix_web::web::resource(&settings_srv.idp.jwks_route).route(actix_web::web::get().to(
                move |data: actix_web::web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    actix_web::HttpResponse::build(actix_web::http::StatusCode::OK)
                },
            )),
        )
    });
    settings.idp.jwks_route = srv.url(&settings.idp.jwks_route);
    let keys = request_jwks(&reqwest::Client::new(), &settings.idp.jwks_route).await;
    assert_eq!(
        keys,
        JsonWebKeyStore::default(),
        "returned a different set of jwk"
    );
    let counter = call_counter.lock().expect("Counter lock failed");
    assert_eq!(1, *counter, "Call occurrence mismatch");
    srv.stop().await;
}

#[actix_rt::test]
async fn ok() {
    let call_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let counter_clone = call_counter.clone();
    let mut settings = super::extract_settings(super::SETTINGS);
    let settings_srv = settings.clone();
    let expected = JsonWebKeyStoreUnparsed {
        keys: vec![
            JsonWebKeyUnparsed {
                kid: String::from("helloiamakey"),
                kty: String::from("RSA"),
                alg: String::from("RS256"),
                usecase: String::from("sign"),
                n: String::from("123"),
                e: String::from("456"),
            },
            JsonWebKeyUnparsed {
                kid: String::from("helloiamakey2"),
                kty: String::from("RSA"),
                alg: String::from("RS512"),
                usecase: String::from("sign"),
                n: String::from("789"),
                e: String::from("101112"),
            },
        ],
    };
    let response = expected.clone();
    let srv = actix_web::test::start(move || {
        let resp_2 = response.clone();
        actix_web::App::new().data(counter_clone.clone()).service(
            actix_web::web::resource(&settings_srv.idp.jwks_route).route(actix_web::web::get().to(
                move |data: actix_web::web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    actix_web::HttpResponse::build(actix_web::http::StatusCode::OK)
                        .json(resp_2.clone())
                },
            )),
        )
    });
    settings.idp.jwks_route = srv.url(&settings.idp.jwks_route);
    let resp = request_jwks(&reqwest::Client::new(), &settings.idp.jwks_route).await;
    assert_eq!(resp.keys.len(), 2, "returned a different set of jwk");
    let counter = call_counter.lock().expect("Counter lock failed");
    assert_eq!(1, *counter, "Call occurrence mismatch");
    srv.stop().await;
}
