use crate::authz::{find_jwk_from_token, JWTClaimsAndBaseFields};
use chrono::{Duration, Utc};
use std::ops::{Add, Sub};

#[test]
fn incoherent_token() {
    let store = vec![super::gen_json_web_key(String::from("RS512"))];
    let res = find_jwk_from_token(
        "incoherent mess: 233^%##45123rwef23434214tety456754h&*#%&^",
        &store,
    );
    assert!(res.is_err(), "Invalid token should return an error");
}

#[test]
fn empty_store() {
    let store = Vec::new();
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::RS512);
    header.kid = Some("test_key".to_owned());
    let claims = JWTClaimsAndBaseFields {
        aud: String::from("iqiper"),
        iss: String::from("iqiper"),
        iat: Utc::now(),
        nbf: Some(Utc::now().sub(Duration::days(1))),
        exp: Some(Utc::now().add(Duration::days(1))),
        sub: String::from("user"),
        scope: String::from("resource.2"),
    };
    let key = super::get_jwt_private_key();
    let token = jsonwebtoken::encode(&header, &claims, &key).expect("Failed encoding");
    let res = find_jwk_from_token(&token, &store);
    assert!(res.is_err(), "An empty can't return a jwk");
}

#[test]
fn no_match() {
    let store = vec![super::gen_json_web_key_wrong(String::from("RS512"))];
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::RS512);
    header.kid = Some("test_key".to_owned());
    let claims = JWTClaimsAndBaseFields {
        aud: String::from("iqiper"),
        iss: String::from("iqiper"),
        iat: Utc::now(),
        nbf: Some(Utc::now().sub(Duration::days(1))),
        exp: Some(Utc::now().add(Duration::days(1))),
        sub: String::from("user"),
        scope: String::from("resource.2"),
    };
    let key = super::get_jwt_private_key();
    let token = jsonwebtoken::encode(&header, &claims, &key).expect("Failed encoding");
    let res = find_jwk_from_token(&token, &store);
    assert!(
        res.is_err(),
        "This store should not contain the correct jwk"
    );
}

#[test]
fn ok() {
    let store = vec![
        super::gen_json_web_key_wrong(String::from("RS512")),
        super::gen_json_web_key(String::from("RS512")),
    ];
    let mut header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::RS512);
    header.kid = Some("test_key".to_owned());
    let claims = JWTClaimsAndBaseFields {
        aud: String::from("iqiper"),
        iss: String::from("iqiper"),
        iat: Utc::now(),
        nbf: Some(Utc::now().sub(Duration::days(1))),
        exp: Some(Utc::now().add(Duration::days(1))),
        sub: String::from("user"),
        scope: String::from("resource.2"),
    };
    let key = super::get_jwt_private_key();
    let token = jsonwebtoken::encode(&header, &claims, &key).expect("Failed encoding");
    let res = find_jwk_from_token(&token, &store);
    let expected = super::gen_json_web_key(String::from("RS512"));
    assert_eq!(
        *res.expect("This store should contains the correct jwk"),
        expected,
        "The returned jwk do not match the correct one"
    );
}
