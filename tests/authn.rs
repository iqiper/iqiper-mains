use iqiper_mains;

use actix_web::{http, test, web, HttpResponse};
use serde::{Deserialize, Serialize};
use std::sync::{Arc, Mutex};

#[macro_use]
mod common;
use common::REFRESH_JWKS_ROUTE;

#[actix_rt::test]
async fn missing_code() {
    let settings = common::extract_config(common::CONFIG);
    let uri = settings.local_server.local_code_route.clone();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_actix_jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = test::TestRequest::get()
        .uri(&format!(
            "{}?grant_type={}&session_state={}",
            uri,
            String::from("authorization_code"),
            String::from("HelloWorld")
        ))
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        http::StatusCode::BAD_REQUEST,
        "Bad status code returned"
    );
}

#[actix_rt::test]
async fn server_error_from_idp() {
    let idp_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let idp_clone = idp_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let srv = test::start(move || {
        actix_web::App::new().data(idp_clone.clone()).service(
            web::resource(&settings_srv.idp.token_route).route(web::post().to(
                |data: web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    HttpResponse::InternalServerError()
                },
            )),
        )
    });
    let url = srv.url(&settings.idp.token_route);
    settings.idp.token_route = url;
    let uri = settings.local_server.local_code_route.clone();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_actix_jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = test::TestRequest::get()
        .uri(&format!(
            "{}?grant_type={}&code={}&session_state={}",
            uri,
            String::from("authorization_code"),
            String::from("HelloCode"),
            String::from("HelloState")
        ))
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        http::StatusCode::SERVICE_UNAVAILABLE,
        "Bad status code returned"
    );
    let counter = idp_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    srv.stop().await;
}

#[actix_rt::test]
async fn user_error_from_idp() {
    let idp_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let idp_clone = idp_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let srv = test::start(move || {
        actix_web::App::new().data(idp_clone.clone()).service(
            web::resource(&settings_srv.idp.token_route).route(web::post().to(
                |data: web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    HttpResponse::BadRequest()
                },
            )),
        )
    });
    let url = srv.url(&settings.idp.token_route);
    settings.idp.token_route = url;
    let uri = settings.local_server.local_code_route.clone();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_actix_jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = test::TestRequest::get()
        .uri(&format!(
            "{}?grant_type={}&code={}&session_state={}",
            uri,
            String::from("authorization_code"),
            String::from("HelloCode"),
            String::from("HelloState")
        ))
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        http::StatusCode::BAD_REQUEST,
        "Bad status code returned"
    );
    let counter = idp_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    srv.stop().await;
}

#[actix_rt::test]
async fn unavailable_from_idp() {
    let mut settings = common::extract_config(common::CONFIG);
    settings.idp.token_route = String::from("http://localhost:1/.idp/token");
    let uri = settings.local_server.local_code_route.clone();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_actix_jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = test::TestRequest::get()
        .uri(&format!(
            "{}?grant_type={}&code={}&session_state={}",
            uri,
            String::from("authorization_code"),
            String::from("HelloCode"),
            String::from("HelloState")
        ))
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        http::StatusCode::SERVICE_UNAVAILABLE,
        "Bad status code returned"
    );
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct IdpMessage {
    message: String,
    toto: String,
}

#[derive(Debug, Deserialize, Serialize, PartialEq, Clone)]
struct AuthorizationCodeFlowReqValue {
    grant_type: String,
    code: String,
    code_verifier: Option<String>,
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
struct AuthorizationCodeFlowFormValue {
    grant_type: String,
    code: String,
    code_verifier: Option<String>,
    client_id: String,
    client_secret: String,
    redirect_uri: String,
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
struct RefreshTokenFlowFormValue {
    grant_type: String,
    refresh_token: String,
    client_id: String,
    client_secret: String,
}

#[derive(Debug, Deserialize, PartialEq, Clone)]
struct PasswordFlowFormValue {
    grant_type: String,
    username: String,
    password: String,
    scope: String,
    client_id: String,
    client_secret: String,
}

#[actix_rt::test]
async fn ok_from_idp() {
    let idp_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let idp_clone = idp_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let srv = test::start(move || {
        let expected = AuthorizationCodeFlowFormValue {
            grant_type: String::from("authorization_code"),
            code: String::from("HelloCode"),
            client_id: settings_srv.idp.client_id.clone(),
            client_secret: settings_srv.idp.client_secret.clone(),
            code_verifier: None,
            redirect_uri: format!(
                "{}://{}:{}{}",
                settings_srv.local_server.advertise_scheme,
                settings_srv.local_server.advertise_uri,
                settings_srv.local_server.port,
                settings_srv.local_server.local_code_route
            ),
        };
        actix_web::App::new().data(idp_clone.clone()).service(
            web::resource(&settings_srv.idp.token_route).route(web::post().to(
                move |body: web::Form<AuthorizationCodeFlowFormValue>,
                      data: web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    assert_eq!(
                        body.into_inner(),
                        expected,
                        "Struct mismatch when requesting token to IdP"
                    );
                    let msg: IdpMessage = IdpMessage {
                        message: String::from("Salut"),
                        toto: String::from("tutu"),
                    };
                    HttpResponse::build(http::StatusCode::OK).json(msg)
                },
            )),
        )
    });
    let url = srv.url(&settings.idp.token_route);
    settings.idp.token_route = url;
    let uri = settings.local_server.local_code_route.clone();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_actix_jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = test::TestRequest::get()
        .uri(&format!(
            "{}?grant_type={}&code={}&session_state={}",
            uri,
            String::from("authorization_code"),
            String::from("HelloCode"),
            String::from("HelloState")
        ))
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        http::StatusCode::OK,
        "Bad status code returned"
    );
    let counter = idp_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    let body_raw = test::read_body(resp).await;
    let body_str = std::str::from_utf8(&body_raw).expect("Error body badly encoded");
    let body_resp: IdpMessage = serde_json::from_str(&body_str).expect("Json format incorrect");
    let expected = IdpMessage {
        message: String::from("Salut"),
        toto: String::from("tutu"),
    };
    assert_eq!(body_resp, expected, "Idp message do not match");
    srv.stop().await;
}

#[actix_rt::test]
async fn ok_from_idp_refresh_token() {
    let idp_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let idp_clone = idp_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let srv = test::start(move || {
        let expected = RefreshTokenFlowFormValue {
            grant_type: String::from("refresh_token"),
            refresh_token: String::from("HELLOIAMAREFRESHTOKEN"),
            client_id: settings_srv.idp.client_id.clone(),
            client_secret: settings_srv.idp.client_secret.clone(),
        };
        actix_web::App::new().data(idp_clone.clone()).service(
            web::resource(&settings_srv.idp.token_route).route(web::post().to(
                move |body: web::Form<RefreshTokenFlowFormValue>,
                      data: web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    assert_eq!(
                        body.into_inner(),
                        expected,
                        "Struct mismatch when requesting token to IdP"
                    );
                    let msg: IdpMessage = IdpMessage {
                        message: String::from("Salut"),
                        toto: String::from("tutu"),
                    };
                    HttpResponse::build(http::StatusCode::OK).json(msg)
                },
            )),
        )
    });
    let url = srv.url(&settings.idp.token_route);
    settings.idp.token_route = url;
    let uri = settings.local_server.local_code_route.clone();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_actix_jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = test::TestRequest::get()
        .uri(&format!(
            "{}?grant_type={}&refresh_token={}",
            uri,
            String::from("refresh_token"),
            String::from("HELLOIAMAREFRESHTOKEN"),
        ))
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        http::StatusCode::OK,
        "Bad status code returned"
    );
    let counter = idp_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    let body_raw = test::read_body(resp).await;
    let body_str = std::str::from_utf8(&body_raw).expect("Error body badly encoded");
    let body_resp: IdpMessage = serde_json::from_str(&body_str).expect("Json format incorrect");
    let expected = IdpMessage {
        message: String::from("Salut"),
        toto: String::from("tutu"),
    };
    assert_eq!(body_resp, expected, "Idp message do not match");
    srv.stop().await;
}

#[actix_rt::test]
async fn ok_from_idp_password() {
    let idp_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let idp_clone = idp_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let srv = test::start(move || {
        let expected = PasswordFlowFormValue {
            grant_type: String::from("password"),
            username: String::from("hello_username"),
            password: String::from("hello_password"),
            scope: String::from("hello_scope"),
            client_id: settings_srv.idp.client_id.clone(),
            client_secret: settings_srv.idp.client_secret.clone(),
        };
        actix_web::App::new().data(idp_clone.clone()).service(
            web::resource(&settings_srv.idp.token_route).route(web::post().to(
                move |body: web::Form<PasswordFlowFormValue>, data: web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    assert_eq!(
                        body.into_inner(),
                        expected,
                        "Struct mismatch when requesting token to IdP"
                    );
                    let msg: IdpMessage = IdpMessage {
                        message: String::from("Salut"),
                        toto: String::from("tutu"),
                    };
                    HttpResponse::build(http::StatusCode::OK).json(msg)
                },
            )),
        )
    });
    let url = srv.url(&settings.idp.token_route);
    settings.idp.token_route = url;
    let uri = settings.local_server.local_code_route.clone();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_actix_jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = test::TestRequest::get()
        .header("Content-Type", "application/x-www-form-urlencoded")
        .uri(&format!(
            "{}?grant_type={}&username={}&password={}&scope={}",
            uri,
            String::from("password"),
            String::from("hello_username"),
            String::from("hello_password"),
            String::from("hello_scope"),
        ))
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    // println!("{:#?}", test::read_body(resp).await);
    assert_eq!(
        resp.status(),
        http::StatusCode::OK,
        "Bad status code returned"
    );
    let counter = idp_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    let body_raw = test::read_body(resp).await;
    let body_str = std::str::from_utf8(&body_raw).expect("Error body badly encoded");
    let body_resp: IdpMessage = serde_json::from_str(&body_str).expect("Json format incorrect");
    let expected = IdpMessage {
        message: String::from("Salut"),
        toto: String::from("tutu"),
    };
    assert_eq!(body_resp, expected, "Idp message do not match");
    srv.stop().await;
}

#[actix_rt::test]
async fn ok_from_idp_form() {
    let idp_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let idp_clone = idp_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let srv = test::start(move || {
        let expected = AuthorizationCodeFlowFormValue {
            grant_type: String::from("authorization_code"),
            code: String::from("HelloCode"),
            code_verifier: Some(String::from("hello_verify")),
            client_id: settings_srv.idp.client_id.clone(),
            client_secret: settings_srv.idp.client_secret.clone(),
            redirect_uri: format!(
                "{}://{}:{}{}",
                settings_srv.local_server.advertise_scheme,
                settings_srv.local_server.advertise_uri,
                settings_srv.local_server.port,
                settings_srv.local_server.local_code_route
            ),
        };
        actix_web::App::new().data(idp_clone.clone()).service(
            web::resource(&settings_srv.idp.token_route).route(web::post().to(
                move |body: web::Form<AuthorizationCodeFlowFormValue>,
                      data: web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    assert_eq!(
                        body.into_inner(),
                        expected,
                        "Struct mismatch when requesting token to IdP"
                    );
                    let msg: IdpMessage = IdpMessage {
                        message: String::from("Salut"),
                        toto: String::from("tutu"),
                    };
                    HttpResponse::build(http::StatusCode::OK).json(msg)
                },
            )),
        )
    });
    let url = srv.url(&settings.idp.token_route);
    settings.idp.token_route = url;
    let uri = settings.local_server.local_code_route.clone();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_actix_jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = test::TestRequest::post()
        .uri(uri.as_str())
        .set_form(&AuthorizationCodeFlowReqValue {
            grant_type: String::from("authorization_code"),
            code: String::from("HelloCode"),
            code_verifier: Some(String::from("hello_verify")),
        })
        .to_request();
    let resp = test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        http::StatusCode::OK,
        "Bad status code returned"
    );
    let counter = idp_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    let body_raw = test::read_body(resp).await;
    let body_str = std::str::from_utf8(&body_raw).expect("Error body badly encoded");
    let body_resp: IdpMessage = serde_json::from_str(&body_str).expect("Json format incorrect");
    let expected = IdpMessage {
        message: String::from("Salut"),
        toto: String::from("tutu"),
    };
    assert_eq!(body_resp, expected, "Idp message do not match");
    srv.stop().await;
}
