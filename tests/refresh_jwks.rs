use iqiper_mains;

use iqiper_mains::{JsonWebKeyStoreUnparsed, JsonWebKeyUnparsed};
use std::sync::{Arc, Mutex};
#[macro_use]
mod common;
use common::REFRESH_JWKS_ROUTE;
use iqiper_actix_jwt_extractor::JWTConfig;

#[actix_rt::test]
async fn empty() {
    let call_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let counter_clone = call_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let srv = actix_web::test::start(move || {
        actix_web::App::new().data(counter_clone.clone()).service(
            actix_web::web::resource(&settings_srv.idp.jwks_route).route(actix_web::web::get().to(
                move |data: actix_web::web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    actix_web::HttpResponse::InternalServerError()
                },
            )),
        )
    });
    settings.idp.jwks_route = srv.url(&settings.idp.jwks_route);
    let keys: std::collections::HashMap<String, jsonwebtoken::DecodingKey<'_>> = vec![
        (
            "toto".to_string(),
            jsonwebtoken::DecodingKey::from_secret(b"toto"),
        ),
        (
            "tutu".to_string(),
            jsonwebtoken::DecodingKey::from_secret(b"tutu"),
        ),
    ]
    .into_iter()
    .collect();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_mains::jwt_extractor::JsonWebKeyStore { keys },
        reqwest::Client::new(),
    );
    let mut app = actix_web::test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = actix_web::test::TestRequest::get()
        .uri(&REFRESH_JWKS_ROUTE)
        .to_request();
    let resp = actix_web::test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        actix_web::http::StatusCode::INTERNAL_SERVER_ERROR,
        "Bad status code returned"
    );
    let counter = call_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    assert_eq!(
        conf.jwt_config()
            .jwks()
            .read()
            .expect("the lock")
            .keys
            .len(),
        0,
        "JWKS len don't match"
    );
    srv.stop().await;
}

#[actix_rt::test]
async fn replace() {
    let call_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let counter_clone = call_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let expected = JsonWebKeyStoreUnparsed {
        keys: vec![
            JsonWebKeyUnparsed {
                kid: String::from("helloiamakey"),
                kty: String::from("RSA"),
                alg: String::from("RS256"),
                usecase: String::from("sign"),
                n: String::from("123"),
                e: String::from("456"),
            },
            JsonWebKeyUnparsed {
                kid: String::from("helloiamakey2"),
                kty: String::from("RSA"),
                alg: String::from("RS512"),
                usecase: String::from("sign"),
                n: String::from("789"),
                e: String::from("101112"),
            },
        ],
    };
    let response = expected.clone();
    let srv = actix_web::test::start(move || {
        let resp_2 = response.clone();
        actix_web::App::new().data(counter_clone.clone()).service(
            actix_web::web::resource(&settings_srv.idp.jwks_route).route(actix_web::web::get().to(
                move |data: actix_web::web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    actix_web::HttpResponse::build(actix_web::http::StatusCode::OK)
                        .json(resp_2.clone())
                },
            )),
        )
    });
    settings.idp.jwks_route = srv.url(&settings.idp.jwks_route);
    let keys: std::collections::HashMap<String, jsonwebtoken::DecodingKey<'_>> = vec![(
        "helloiamakey".to_string(),
        jsonwebtoken::DecodingKey::from_secret(b"toto"),
    )]
    .into_iter()
    .collect();
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_mains::jwt_extractor::JsonWebKeyStore { keys },
        reqwest::Client::new(),
    );
    let mut app = actix_web::test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = actix_web::test::TestRequest::get()
        .uri(&REFRESH_JWKS_ROUTE)
        .to_request();
    let resp = actix_web::test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        actix_web::http::StatusCode::OK,
        "Bad status code returned"
    );
    let counter = call_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    assert_eq!(
        conf.jwt_config()
            .jwks()
            .read()
            .expect("The lock")
            .keys
            .len(),
        2,
        "JWKS len don't match"
    );
    srv.stop().await;
}

#[actix_rt::test]
async fn create() {
    let call_counter: Arc<Mutex<u32>> = Arc::new(Mutex::new(0));
    let counter_clone = call_counter.clone();
    let mut settings = common::extract_config(common::CONFIG);
    let settings_srv = settings.clone();
    let expected = JsonWebKeyStoreUnparsed {
        keys: vec![
            JsonWebKeyUnparsed {
                kid: String::from("helloiamakey"),
                kty: String::from("RSA"),
                alg: String::from("RS256"),
                usecase: String::from("sign"),
                n: String::from("123"),
                e: String::from("456"),
            },
            JsonWebKeyUnparsed {
                kid: String::from("helloiamakey2"),
                kty: String::from("RSA"),
                alg: String::from("RS512"),
                usecase: String::from("sign"),
                n: String::from("789"),
                e: String::from("101112"),
            },
        ],
    };
    let response = expected.clone();
    let srv = actix_web::test::start(move || {
        let resp_2 = response.clone();
        actix_web::App::new().data(counter_clone.clone()).service(
            actix_web::web::resource(&settings_srv.idp.jwks_route).route(actix_web::web::get().to(
                move |data: actix_web::web::Data<Arc<Mutex<u32>>>| {
                    let mut counter = data.lock().expect("Counter lock failed");
                    *counter += 1;
                    actix_web::HttpResponse::build(actix_web::http::StatusCode::OK)
                        .json(resp_2.clone())
                },
            )),
        )
    });
    settings.idp.jwks_route = srv.url(&settings.idp.jwks_route);
    let conf = iqiper_mains::AuthConfig::new(
        settings,
        iqiper_mains::jwt_extractor::JsonWebKeyStore::default(),
        reqwest::Client::new(),
    );
    let mut app = actix_web::test::init_service(iqiper_service_mains_app!(conf)).await;
    let req = actix_web::test::TestRequest::get()
        .uri(&REFRESH_JWKS_ROUTE)
        .to_request();
    let resp = actix_web::test::call_service(&mut app, req).await;
    assert_eq!(
        resp.status(),
        actix_web::http::StatusCode::OK,
        "Bad status code returned"
    );
    let counter = call_counter.lock().expect("Counter lock failed");
    assert_eq!(*counter, 1, "Idp endpoint called occurence mismatch");
    assert_eq!(
        conf.jwt_config()
            .jwks()
            .read()
            .expect("The lock")
            .keys
            .len(),
        2,
        "JWKS len don't match"
    );
    srv.stop().await;
}
