#![allow(dead_code)]

use iqiper_mains;

use jsonwebtoken::EncodingKey;
use serde::Deserialize;
use std::fs::File;
use std::io::Read;
pub const REFRESH_JWKS_ROUTE: &'static str = "/jwks/refresh";
pub const CONFIG: &str = "IQIPER_MAINS_CONFIG";
pub const API: &str = "IQIPER_API_FILE";

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
enum ReceivedMessage {
    Error(ReceivedError),
}

#[derive(Deserialize)]
pub struct ReceivedError {
    pub code: u16,
    pub message: String,
    #[serde(default)]
    pub wanted_scope: Vec<String>,
}

pub async fn extract_error(resp: actix_web::dev::ServiceResponse) -> ReceivedError {
    let body_raw = actix_web::test::read_body(resp).await;
    let body_str = std::str::from_utf8(&body_raw).expect("Error body badly encoded");
    let body_resp: ReceivedMessage =
        serde_json::from_str(&body_str).expect("Json format incorrect");
    let ReceivedMessage::Error(err) = body_resp;
    err
}

pub fn extract_config(var_name: &str) -> iqiper_mains::AuthSettings {
    let path = std::env::var(var_name).expect("Env var is missing");
    let file = std::fs::File::open(&path).expect("File is missing");
    let reader = std::io::BufReader::new(file);
    serde_yaml::from_reader(reader).expect("Deserialization failed")
}

#[iqiper_mains::iqiper_mains_route(method = "get", route = "/resource/0")]
pub async fn resource_0() -> actix_web::HttpResponse {
    actix_web::HttpResponse::Ok().into()
}

#[iqiper_mains::iqiper_mains_route(method = "get", route = "/resource/1")]
pub async fn resource_1() -> actix_web::HttpResponse {
    actix_web::HttpResponse::Ok().into()
}

#[iqiper_mains::iqiper_mains_route(method = "get", route = "/resource/2")]
pub async fn resource_2() -> actix_web::HttpResponse {
    actix_web::HttpResponse::Ok().into()
}

#[iqiper_mains::iqiper_mains_route(method = "get", route = "/resource/3")]
pub async fn resource_3() -> actix_web::HttpResponse {
    actix_web::HttpResponse::Ok().into()
}

#[macro_export]
macro_rules! iqiper_service_mains_app {
    ($conf:expr) => {
        actix_web::App::new()
            .app_data(
                <iqiper_mains::QueryGrant as actix_web::FromRequest>::configure(
                    iqiper_mains::query_handler,
                ),
            )
            .default_service(actix_web::web::route().to(|| actix_web::HttpResponse::NotFound()))
            .route(
                &$conf.settings().local_server.local_code_route,
                actix_web::web::get().to(iqiper_mains::forward_authn_token_query),
            )
            .route(
                &$conf.settings().local_server.local_code_route,
                actix_web::web::post().to(iqiper_mains::forward_authn_token_form),
            )
            .route(
                &REFRESH_JWKS_ROUTE,
                actix_web::web::get().to(iqiper_mains::refresh_jwks),
            )
            .data($conf.clone());
    };
    ($conf:expr, $api:expr) => {
        actix_web::App::new()
            .app_data(
                <iqiper_mains::QueryGrant as actix_web::FromRequest>::configure(
                    iqiper_mains::query_handler,
                ),
            )
            .default_service(actix_web::web::route().to(|| actix_web::HttpResponse::NotFound()))
            .route(
                &$conf.settings().local_server.local_code_route,
                actix_web::web::get().to(iqiper_mains::forward_authn_token_query),
            )
            .route(
                &$conf.settings().local_server.local_code_route,
                actix_web::web::post().to(iqiper_mains::forward_authn_token_form),
            )
            .route(
                &REFRESH_JWKS_ROUTE,
                actix_web::web::get().to(iqiper_mains::refresh_jwks),
            )
            .service(common::resource_0)
            .service(common::resource_1)
            .service(common::resource_2)
            .service(common::resource_3)
            .data($conf.clone())
            .data($api.clone());
    };
}

pub fn get_jwt_private_key() -> EncodingKey {
    let path = std::env::var("IQIPER_KEY_TEST_PATH").expect("Missing env ver for test");
    let mut file = File::open(format!("{}/{}", path, String::from("jwt.private")))
        .expect("Failed to open test file for secret key");
    let mut buffer: Vec<u8> = Vec::new();
    file.read_to_end(&mut buffer).expect("Read fail");
    EncodingKey::from_rsa_pem(buffer.as_slice()).expect("Failed encoding key")
}
