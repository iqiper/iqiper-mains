extern crate proc_macro;
use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn;

const FORBIDDEN_CHAR: [char; 4] = ['/', '{', '}', '-'];

fn extract_arg(lst: &syn::AttributeArgs, id: &str) -> Option<String> {
    lst.iter().find_map(|nested_meta| {
        if let syn::NestedMeta::Meta(meta) = nested_meta {
            match meta {
                syn::Meta::NameValue(val) => {
                    let seg = &val.path.segments;
                    if seg.len() == 1 && seg.first()?.ident == id {
                        match &val.lit {
                            syn::Lit::Str(lit) => Some(lit.value()),
                            _ => None,
                        }
                    } else {
                        None
                    }
                }
                _ => None,
            }
        } else {
            None
        }
    })
}

/// Create a route handler with the middleware of `iqiper-mains` for the authz.
/// 
/// # Input
/// A function that will be used to handle the route.
/// 
/// # Attributes
/// * `method="method_name"` - Raw literal string representing the method to use for this handler.
/// * `route="/route/to/handle"` - Raw literal string with path for which to register handler.
#[proc_macro_attribute]
pub fn iqiper_mains_route(args: TokenStream, input: TokenStream) -> TokenStream {
    let args = syn::parse_macro_input!(args as syn::AttributeArgs);
    let method_str = extract_arg(&args, "method").expect("method not correctly specified");
    let method = format_ident!("{}", method_str);
    let route = extract_arg(&args, "route").expect("method not correctly specified");
    let wrapper_name =
        format!("wrp_{}_{}", method.to_string(), route).replace(&FORBIDDEN_CHAR[..], "_");
    let wrapper_id = format_ident!("{}", wrapper_name);
    let wrapper = format!(
        "iqiper_mains::actix_web_httpauth::middleware::HttpAuthentication::with_fn({})",
        wrapper_name
    );
    let input = syn::parse_macro_input!(input as syn::ItemFn);
    let output = quote! {
        async fn #wrapper_id (
            req: actix_web::dev::ServiceRequest,
            jwt: iqiper_mains::jwt_extractor::CoreJWTAuth<iqiper_mains::JWTClaims>,
        ) -> Result<actix_web::dev::ServiceRequest, actix_web::Error> {
            iqiper_mains::validator(req, jwt, #route, #method_str)
        }

        #[actix_web::#method(#route, wrap=#wrapper)]
        #input
    };
    output.into()
}
